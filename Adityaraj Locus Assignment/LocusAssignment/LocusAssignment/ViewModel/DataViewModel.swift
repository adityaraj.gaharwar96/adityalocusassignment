//
//  DataViewModel.swift
//  LocusAssignment
//
//  Created by Adityaraj Singh on 08/08/22.
//

import Foundation

class DataViewModel {
    typealias ResponseCompletion = (DataModel) -> ()
    func loadJson(fileName: String, completion: @escaping ResponseCompletion){
       let decoder = JSONDecoder()
       guard
            let url = Bundle.main.url(forResource: fileName, withExtension: "json"),
            let data = try? Data(contentsOf: url),
            let dataArray = try? decoder.decode(DataModel.self, from: data)
       else {
            return completion([])
       }

       return completion(dataArray)
    }
    
}
