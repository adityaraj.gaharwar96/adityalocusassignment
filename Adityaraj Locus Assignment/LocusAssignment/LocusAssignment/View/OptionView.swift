//
//  OptionView.swift
//  LocusAssignment
//
//  Created by Adityaraj Singh on 08/08/22.
//

import UIKit

class OptionView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var btnOption: UIButton!
   
    var optionTap: (() -> ())?
    
    override init(frame: CGRect) {// for using custom view in code
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {// for using custom view in IB
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("OptionView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    @IBAction func btnOptionTap(_ sender: UIButton) {
        optionTap?()
    }
    
}
