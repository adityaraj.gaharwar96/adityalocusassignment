//
//  CommentCell.swift
//  LocusAssignment
//
//  Created by Adityaraj Singh on 08/08/22.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var lblCommentTitle: UILabel!
    @IBOutlet weak var txtViewComment: UITextView!
    @IBOutlet weak var heightTxtView: NSLayoutConstraint!
    @IBOutlet weak var switchComment: UISwitch!
    
    var switchTap: ((UISwitch) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func swtichOnTap(_ sender: UISwitch) {
        switchTap?(sender)
    }
    
}
