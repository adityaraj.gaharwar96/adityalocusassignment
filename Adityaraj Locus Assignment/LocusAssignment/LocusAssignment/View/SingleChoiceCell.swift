//
//  SingleChoiceCell.swift
//  LocusAssignment
//
//  Created by Adityaraj Singh on 08/08/22.
//

import UIKit

class SingleChoiceCell: UITableViewCell {

    @IBOutlet weak var lblChoiceTitle: UILabel!
    @IBOutlet weak var stkView: UIStackView!
    
    var choiceData: DataModelElement? {
        didSet {
            lblChoiceTitle.text = choiceData?.title ?? ""
            stkView.arrangedSubviews.forEach { view in
                stkView.removeArrangedSubview(view)
            }
            if let opts = choiceData?.dataMap?.options, opts.count > 0 {
                for i in 0..<opts.count {
                    let optView = OptionView()
                    optView.btnOption.tag = i
                    optView.btnOption.setTitle(opts[i], for: .normal)
                    stkView.addArrangedSubview(optView)
                }
//                opts.forEach({ optionName in
//                    let optView = OptionView()
//                    optView.btnOption.setTitle(optionName, for: .normal)
//                    stkView.addArrangedSubview(optView)
//                })
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
