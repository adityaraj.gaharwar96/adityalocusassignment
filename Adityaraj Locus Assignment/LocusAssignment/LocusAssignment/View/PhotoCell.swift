//
//  PhotoCell.swift
//  LocusAssignment
//
//  Created by Adityaraj Singh on 08/08/22.
//

import UIKit

class PhotoCell: UITableViewCell {

    @IBOutlet weak var lblPhotoTitle: UILabel!
    @IBOutlet weak var imgPhoto: UIImageView!
    
    var btnCameraTap: (() -> ())?
    var btnDeleteTap: (() -> ())?
    var photoData : DataModelElement? {
        didSet {
            lblPhotoTitle.text = photoData?.title ?? ""
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnRemovePhotoTap(_ sender: UIButton) {
        btnDeleteTap?()
    }
    
    @IBAction func btnOpenCameraTap(_ sender: UIButton) {
        btnCameraTap?()
    }
    
}
