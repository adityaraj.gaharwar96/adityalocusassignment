//
//  ViewController.swift
//  LocusAssignment
//
//  Created by Adityaraj Singh on 02/08/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    
    var dataArr : DataModel?
    var responses = ResponseData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerXibs()
        getJsonData()
    }
    
    @IBAction func btnSubmitTap(_ sender: UIBarButtonItem) {
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResponseVC") as? ResponseVC else { return }
        vc.navigationController?.isNavigationBarHidden = false
        vc.response = responses
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ViewController : UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    func registerXibs() {
        self.tblView.register(UINib(nibName: "PhotoCell", bundle: nil), forCellReuseIdentifier: "PhotoCell")
        self.tblView.register(UINib(nibName: "SingleChoiceCell", bundle: nil), forCellReuseIdentifier: "SingleChoiceCell")
        self.tblView.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: "CommentCell")
    }
    
    func getJsonData() {
        let viewModel = DataViewModel()
        viewModel.loadJson(fileName: "Data") { responseArr in
            self.dataArr = responseArr
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            DispatchQueue.main.async {
                self.tblView.reloadData()
            }
            return false
        }
        guard let dataElement = dataArr?[textView.tag] else { return true }
        if let row = self.responses.responseArr.firstIndex(where: {$0.ids == dataElement.id}) {
            self.responses.responseArr[row].choices = textView.text
        } else {
            self.responses.responseArr.append(ResponseModelElement(ids: dataElement.id, choices: textView.text))
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let dataElement = dataArr?[indexPath.row] else {
            let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
            return cell
        }
        switch dataElement.type {
        case .comment:
            let commentObj = responses.responseArr.filter {$0.ids == dataElement.id}.first
            let commentSwitch = commentObj?.isCommentSwitchOn
            let comment = commentObj?.choices as? String ?? ""
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell") as! CommentCell
            cell.lblCommentTitle.text = dataElement.title
            cell.txtViewComment.text = comment
            cell.txtViewComment.delegate = self
            cell.txtViewComment.tag = indexPath.row
            cell.switchComment.isOn = commentSwitch ?? true
            print(responses.responseArr)
            cell.switchTap = { sender in
                DispatchQueue.main.async {
                    if let row = self.responses.responseArr.firstIndex(where: {$0.ids == dataElement.id}) {
                        self.responses.responseArr[row].isCommentSwitchOn = sender.isOn
                    } else {
                        if cell.txtViewComment.text != "" {
                            self.responses.responseArr.append(ResponseModelElement(ids: dataElement.id, isCommentSwitchOn: sender.isOn))
                        }
                    }
                    self.tblView.reloadData()
                }
            }
            
            if commentSwitch ?? true {
                cell.heightTxtView.constant = 100
            } else {
                cell.heightTxtView.constant = 0
                cell.txtViewComment.text = ""
                if let row = self.responses.responseArr.firstIndex(where: {$0.ids == dataElement.id}) {
                    //self.responses.responseArr[row].choices = ""
                    self.responses.responseArr.remove(at: row)
                    print(self.responses.responseArr)
                } else {
                self.responses.responseArr.append(ResponseModelElement(ids: dataElement.id, choices: ""))
                }
            }
            return cell
        case .photo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCell") as! PhotoCell
            cell.photoData = dataElement
            cell.btnDeleteTap = {
                if cell.imgPhoto.image != nil, let storedIndex = self.responses.responseArr.firstIndex(where: { element in element.ids == dataElement.id }) {
                    cell.imgPhoto.image = nil
                    self.responses.responseArr.remove(at: storedIndex)
                }
            }
            if let storedIndex = responses.responseArr.firstIndex(where: { element in element.ids == dataElement.id }), let img = responses.responseArr[storedIndex].choices as? UIImage {
                cell.imgPhoto.image = img
            } else {
                cell.imgPhoto.image = nil
            }
            cell.btnCameraTap = {
                CameraPickerManager().pickImage(self) { image in
                    cell.imgPhoto.image = image
                    let element = ResponseModelElement(ids: dataElement.id, choices: image as Any)
                    self.responses.responseArr.append(element)
                    print(self.responses)
                }
            }
            return cell
        case .singleChoice:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SingleChoiceCell") as! SingleChoiceCell
            cell.choiceData = dataElement
            cell.stkView.arrangedSubviews.forEach { views in
                if let optView = views as? OptionView {
                    if let storedIndex = self.responses.responseArr.firstIndex(where: { element in element.ids == dataElement.id }) {
                        if optView.btnOption.title(for: .normal) == responses.responseArr[storedIndex].choices as? String {
                            optView.btnOption.setImage(UIImage(systemName: "circle.circle.fill"), for: .normal)
                        }
                    }
                    optView.optionTap = {
                        cell.stkView.arrangedSubviews.forEach { subview in
                            if let otherOptView = subview as? OptionView {
                                otherOptView.btnOption.setImage(UIImage(systemName: "circle.circle"), for: .normal)
                            }
                        }
                        if let storedIndex = self.responses.responseArr.firstIndex(where: { element in element.ids == dataElement.id }) {
                            self.responses.responseArr.remove(at: storedIndex)
                        }
                        optView.btnOption.setImage(UIImage(systemName: "circle.circle.fill"), for: .normal)
                        self.responses.responseArr.append(ResponseModelElement(ids: dataElement.id, choices: optView.btnOption.title(for: .normal) ?? ""))
                        print(self.responses.responseArr)
                    }
                }
            }
            return cell
        default:
            let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

