//
//  ResponseModel.swift
//  LocusAssignment
//
//  Created by Adityaraj Singh on 04/08/22.
//

import Foundation

struct ResponseData {
    var responseArr = [ResponseModelElement<Any>]()
}

struct ResponseModelElement<T> {
    var ids: String?
    var choices: T?
    var isCommentSwitchOn: Bool?
}
