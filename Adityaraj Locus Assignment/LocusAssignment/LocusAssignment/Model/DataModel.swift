//
//  DataModel.swift
//  LocusAssignment
//
//  Created by Adityaraj Singh on 03/08/22.
//
import Foundation

// MARK: - DataModelElement
struct DataModelElement: Codable {
    let type: TypeEnum?
    let id, title: String?
    let dataMap: DataMap?
}

// MARK: - DataMap
struct DataMap: Codable {
    let options: [String]?
}

enum TypeEnum: String, Codable {
    case comment = "COMMENT"
    case photo = "PHOTO"
    case singleChoice = "SINGLE_CHOICE"
}

typealias DataModel = [DataModelElement]
