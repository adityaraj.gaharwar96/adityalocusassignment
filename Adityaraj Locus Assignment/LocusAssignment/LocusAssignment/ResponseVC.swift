//
//  ResponseVC.swift
//  LocusAssignment
//
//  Created by Adityaraj Singh on 08/08/22.
//

import UIKit

class ResponseVC: UIViewController {

    @IBOutlet weak var lblResponse: UILabel!
    
    var response : ResponseData?
    var prettyPrint = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        response?.responseArr.forEach({ elemnt in
            if let id = elemnt.ids, let choice = elemnt.choices {
                prettyPrint.append("id:-\(id) --- choice:-\(choice)\n")
            }
        })
        lblResponse.text = prettyPrint
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
